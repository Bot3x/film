import React, { Component } from 'react'
import { View, Text, ScrollView, Image } from 'react-native'


export default class App extends Component {
    constructor(){
        super()
        this.state = {
            data:[]
        }
    }

    componentDidMount = () => {
        fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4')
        .then(response => response.json())
                .then(json => this.setState({data:json.Search}))
                .catch(function(error) {
                    console.log('There has been a problem with your fetch operation: ' + error.message);
                      throw error;
                    });
            }
    

    render() {
        return (
            <View style={{flex:1}}>
                <View style={{backgroundColor: 'black', alignItems: 'center',height:80,justifyContent:'center'}}>
                        <Text style={{ color: 'red',fontSize:30 }}>Avenger Series</Text>
                    </View>
                <ScrollView>
                {this.state.data.map((value,index)=>{
                    return(
                    <View style={{ backgroundColor: 'black',}}key = {index}>
                        <View style={{backgroundColor:'blue',width:350,height:200,borderRadius:20,padding:10,flexDirection:'row',justifyContent:'space-around',alignSelf:'center',marginVertical:15}}>
                            <Image style={{height:'100%',width:'35%',borderRadius:10,}}source={{uri:value.Poster}}/>
                                <View style={{flex:1,justifyContent:'space-between',padding:10}}>
                                    <Text style={{fontSize:25,color:'red'}}>{value.Title}</Text>
                                    <Text style={{fontSize:20,color:'red'}}>{value.Year}</Text>
                                    <Text style={{fontSize:20,color:'red'}}>{value.imdbID}</Text>
                                    </View>
                        </View>
                    </View>
                    )
                })}
                </ScrollView>
            </View>
        )
    }
 }


